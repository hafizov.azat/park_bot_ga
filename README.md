# Общие сведения

Бот предназначен для обнуления парковочных белетов. Боту можно прислать фото билета он распознаст его штрихкод и обнулит белет в БД парковки.

![video](/instructions/video/Video.mp4)

В боте предусмотрены 2 уровня доступа:

- Пользователи бота

- Администраторы бота

Пользователи бота могут только обнулять билеты, всем пользователям у кого нет доступа бот наишет что нет доступа. Пользователю бот предложет выслать фото парковочного билета или написать его номер (Можно сканировать штрих-код через сканер шк подключенный по usb - otg кабель)

![img](/instructions/img/accsess_den.JPG)
![img](/instructions/img/enter_code.JPG)

В случае совпадения типа штрихкода и его длины бот обнавит его в базе. Если бот не сможет распознать штрих-код то выдаст соответсвующее сообщение:

![img](/instructions/img/sussefull.jpg)
![img](/instructions/img/not_found.JPG)

Администраторы бота могут:

- Добавлять пользователей просто скинув контакт пользователя

- Удалять пользователей

- Получать отчет о обнуленных белетах в ексель файл (Информация кто обнулял, номер белета и дата обнуления)

Если администратор пришлет контакт сотрудника то бот добавит его в базу данных как пользователь

![img](/instructions/img/add_user.jpg)

На команду /del_user администраторы могут удалить пользователя

![img](/instructions/img/del_user.JPG)

На команду /get_users администраторы могут получать список пользователей бота

![img](/instructions/img/get_users.jpg)

На команду /report администраторы могут получать отчет 

![img](/instructions/img/report.JPG)
