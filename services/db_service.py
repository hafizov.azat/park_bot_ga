import pymysql
import sqlite3
import json
from datetime import datetime, timedelta


with open(f'config.json') as f:
    read_data = json.load(f)
    host = read_data["db_host"]
    user = read_data["db_user"]
    password = read_data["db_password"]
    db = read_data["db_name"]

def get_connection_mysql():
    return pymysql.connect(host=host,
                           user=user,
                           password=password,
                           db=db,
                           charset='utf8',
                           cursorclass=pymysql.cursors.DictCursor)


def reset_tiket(tiket_num):
    con = get_connection_mysql()
    cursor = con.cursor()

    cursor.execute(f"UPDATE movTransit SET traTarId=0 WHERE traStatus='V' AND traCode='{tiket_num}'")
    con.commit()

    cursor.execute(f"UPDATE movTransit SET trauseid = 19,traDiscountType= 1,traDiscountValue=10000,traNotes='' "
                   f"WHERE traStatus='V' AND traCode='{tiket_num}'")
    con.commit()

    cursor.execute(f"DELETE FROM paypayment WHERE payIdEvent=7 AND payTrauid in (SELECT trauid FROM movtransit "
                   f"WHERE traStatus='V' AND traCode = '{tiket_num}')")
    con.commit()

    # today = datetime.today()
    # paycreation = today.strftime("%Y-%m-%d %H:%M:%S")
    # pay_time_limit = str(today - timedelta(minutes=5)).split('.')[0]
    # year = today.strftime("%Y")
    # cursor.execute(f"select traUID from movtransit where traStatus='V' and traCode = '{tiket_num}'")
    # tra_uid = cursor.fetchall()[0]['traUID']

    # cursor.execute(f"INSERT INTO payPayment ("
    #                f"paycreation,"
    #                f"payuseid,"
    #                f"payUID,"
    #                f"payTraUID,"
    #                f"payYear,"
    #                f"payDateTime,"
    #                f"payIdTerminal,"
    #                f"payEnabled,"
    #                f"payDisableTime,"
    #                f"payTarId,"
    #                f"payTimeLimit,"
    #                f"payType,"
    #                f"payCurrency,"
    #                f"payAmount,"
    #                f"payPayed,"
    #                f"payChange,"
    #                f"payPAN,"
    #                f"paySTAN,"
    #                f"payIdEvent) "
    #                f"VALUES ("
    #                f"'{paycreation}',"
    #                f"19,"
    #                f"4848340425125650,"
    #                f"{tra_uid},"
    #                f"{year},"
    #                f"'{paycreation}',"
    #                f"1,"
    #                f"1,"
    #                f"0,"
    #                f"0,"
    #                f"'{pay_time_limit}',"
    #                f"'W',"
    #                f"'RUB',"
    #                f"0,"
    #                f"0,"
    #                f"0,"
    #                f"'',"
    #                f"'',"
    #                f"7)")
    # con.commit()

    cursor.close()
    con.close()
    

def get_tg_users_id():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM tg_users;")
    tg_users_data = cur.fetchall()
    tg_users = [tg_user[0] for tg_user in tg_users_data]
    conn.close()
    return tg_users


def get_users_info():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM tg_users;")
    tg_users = cur.fetchall()
    conn.close()
    return tg_users

def get_users_info_where_id(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM tg_users WHERE tg_id = ?", (tg_id,))
    tg_users = cur.fetchall()
    conn.close()
    return tg_users[0]


def get_tg_admins_id():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM tg_admin_users;")
    tg_users_data = cur.fetchall()
    tg_users = [tg_user[0] for tg_user in tg_users_data]
    conn.close()
    return tg_users


def get_events():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM events;")
    events = cur.fetchall()
    conn.close()
    return events


def write_user(tg_id, phone_number, first_name, last_name, add_user):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO tg_users(tg_id, phone_number, first_name, last_name, add_users_id) VALUES (?, ?, ?, ?, ?);", (tg_id, phone_number, first_name, last_name, add_user))
        conn.commit()
        result = True
    except sqlite3.IntegrityError:
        result = False
    conn.close()
    return result


def delete_user(phone):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    try:
        cur.execute("DELETE FROM tg_users WHERE phone_number = ?", (phone,))
        conn.commit()
        result = True
    except sqlite3.IntegrityError:
        result = False
    conn.close()
    return result


def write_admin_user(tg_id, phone_number, first_name, last_name):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO tg_admin_users(tg_id, phone_number, first_name, last_name) VALUES (?, ?, ?, ?);", (tg_id, phone_number, first_name, last_name))
        conn.commit()
        result = True
    except sqlite3.IntegrityError:
        result = False
    conn.close()
    return result

def write_event(tiket_num, date_time, user_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO events(tiket_num, date_time, users_id) VALUES (?, ?, ?);", (tiket_num, date_time, user_id))
        conn.commit()
        result = True
    except sqlite3.IntegrityError:
        result = False
    conn.close()
    return result